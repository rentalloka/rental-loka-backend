package controller

import (
	"topindopay/golang-boilerplate/model"
	"topindopay/golang-boilerplate/service"

	"github.com/gofiber/fiber/v2"
)

type ExampleController struct {
	service service.ExampleService
}

func NewExampleController(service *service.ExampleService) ExampleController {
	return ExampleController{service: *service}
}

func (controller ExampleController) Route(app fiber.Router) {
	router := app.Group("/example")

	router.Get("/", controller.Example)
}

func (controller ExampleController) Example(c *fiber.Ctx) error {
	controller.service.Example()

	return c.Status(200).JSON(model.Response{
		Code:   200,
		Status: "OK",
	})
}
