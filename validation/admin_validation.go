package validation

import (
	"encoding/json"
	"topindopay/golang-boilerplate/exception"
	"topindopay/golang-boilerplate/model"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

func ValidateExample(example model.ExampleModel) {
	err := validation.ValidateStruct(&example,
		validation.Field(&example.Name,
			validation.Required.Error("NOT_BLANK")),
	)

	if err != nil {
		b, _ := json.Marshal(err)
		panic(exception.ValidationError{
			Message: string(b),
		})

	}
}
