module topindopay/golang-boilerplate

go 1.16

require (
	github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/joho/godotenv v1.3.0
	github.com/olivere/elastic/v7 v7.0.24
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.13.0 // indirect
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.7
)
