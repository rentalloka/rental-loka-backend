# Boiler Plate

## Redis Helper

#### Set Data

```sh
exp := 1 * time.Minute
helper.InsertRedis(model.SetDataRedis{
	Key:  "example-key",
	Data: newData,
	Exp:  exp,
})
```

#### Get Data

```sh
value := helper.GetRedis(model.Redis{Key: "example-key"})
```

#### Delete Data

```sh
helper.DelRedis(model.Redis{Key: "example-key"})
```

### Elastic Helper

#### Get Data

```sh
var datas []entity.ElasticEntity

searchSource := elastic.NewSearchSource()
searchSource.Query(elastic.NewMatchQuery("name", "example")) ## for querying data

searchResult := helper.GetDataElastic(model.GetDataElastic{
	IndexName:    "example-index",
	SearchSource: searchSource,
})

for _, hit := range searchResult.Hits.Hits {
	var data entity.ElasticEntity
	err := json.Unmarshal(hit.Source, &data)
	exception.PanicIfNeeded(err)
	datas = append(datas, data)
}

fmt.Println(datas)
```

#### Insert Data

```sh
newData := entity.ElasticEntity{Name: "example"}
payloadElastic := model.InsertDataElastic{
	IndexName: indexName,
	Data:      newData,
}
helper.InsertElastic(payloadElastic)
```

### Kafka

#### Publish Message

```sh
helper.PublishMessage(model.PublishModel{
	Topic: "topicname",
	Data:  data,
})
```

### Validation

```sh
func ValidateExample(example model.ExampleModel) {
	err := validation.ValidateStruct(&example,
		validation.Field(&example.Name,
			validation.Required.Error("NOT_BLANK")),
	)

	if err != nil {
		b, _ := json.Marshal(err)
		panic(exception.ValidationError{
			Message: string(b),
		})

	}
}
```

[Click Here for more validation option](https://github.com/go-ozzo/ozzo-validation)

### Unit Test

First go to test folder

```sh
cd /test
```

#### Test All Unit Test

```sh
go test -v
```

#### Test a Single Unit Test

```sh
go test -v -run TEST_FUNCTION
```

example:

```sh
go test -v -run TestUnitController_List
```

### Seeding

```sh
go run main.go seed [Name of Seed]
```
