package main

import (
	"topindopay/golang-boilerplate/config"
	"topindopay/golang-boilerplate/exception"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func main() {
	// Setup Configuration
	configuration := config.New(".env")
	mysql := config.MysqlConnection(configuration)

	// SETUP SEEDER
	// handleArgs(mysql)

	// SETUP REPOSITORIES

	// SETUP SERVICES

	// SETUP CONTROLLERS

	// SETUP FIBER
	app := fiber.New(config.NewFiberConfig())
	app.Use(recover.New())

	// Setup static file for image
	app.Static("/images/courier", "./uploads")

	// SETUP ROUTES
	// v1 := app.Group("/api/v1/plate", middleware.CheckToken())
	v1 := app.Group("/api/v1/plate")

	//route here

	// Start App
	err := app.Listen(configuration.Get("PORT"))
	exception.PanicIfNeeded(err)
}

// func handleArgs(db *gorm.DB) {
// 	flag.Parse()
// 	args := flag.Args()

// 	if len(args) >= 1 {
// 		switch args[0] {
// 		case "seed":

// 			seeds.Execute(db, args[1:]...)
// 			os.Exit(0)
// 		}
// 	}
// }
