package config

import "github.com/go-redis/redis"

func RedisConnection(configuration Config) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     configuration.Get("REDIS_HOST"),
		Password: configuration.Get("REDIS_PASS"),
	})

	return client

}
