package model

import "github.com/olivere/elastic/v7"

type InsertDataElastic struct {
	IndexName string
	Data      interface{}
}

type GetDataElastic struct {
	IndexName    string
	SearchSource *elastic.SearchSource
}
