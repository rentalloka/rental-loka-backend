package model

type PublishModel struct {
	Topic string
	Data  interface{}
}
