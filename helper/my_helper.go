package helper

import (
	"encoding/json"
	"fmt"
	"topindopay/golang-boilerplate/config"
	"topindopay/golang-boilerplate/exception"
	"topindopay/golang-boilerplate/model"

	"golang.org/x/net/context"
)

var ctx = context.Background()
var configuration = config.New(".env")
var redis = config.RedisConnection(configuration)

func InsertRedis(payload model.SetDataRedis) {
	json, err := json.Marshal(payload.Data)
	exception.PanicIfNeeded(err)
	err = redis.Set(payload.Key, json, payload.Exp).Err()
	exception.PanicIfNeeded(err)
}

func GetRedis(payload model.Redis) interface{} {

	value, _ := redis.Get("name").Result()
	if value == "" {
		fmt.Println("key not found")
	}
	return value
}

func DelRedis(payload model.Redis) {
	redis.Del(payload.Key)
}
