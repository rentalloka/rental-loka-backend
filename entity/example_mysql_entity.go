package entity

type Example struct {
	Name string `gorm:"column:name"`
}

func (Example) TableName() string {
	return "example_table"
}
