package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"topindopay/golang-boilerplate/model"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
)

type DecodedStructure struct {
	ResellerId string `json:"idReseller"`
	PricePlan  int32  `json:"pricePlan"`
}

func ValidateToken(encodedToken string) (token *jwt.Token, errData error) {
	jwtPublicKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(os.Getenv("JWT_PUBLIC_KEY")))
	if err != nil {
		return token, err
	}

	tokenString := encodedToken
	claims := jwt.MapClaims{}
	token, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtPublicKey, nil
	})
	if err != nil {
		return token, err
	}
	if !token.Valid {
		return token, errors.New("invalid token")
	}
	return token, nil
}

func DecodeToken(encodedToken string) (decodedResult DecodedStructure, errData error) {
	jwtPublicKey, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(os.Getenv("JWT_PUBLIC_KEY")))

	tokenString := encodedToken
	claims := jwt.MapClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtPublicKey, nil
	})
	if err != nil {
		return decodedResult, err
	}
	if !token.Valid {
		return decodedResult, errors.New("invalid token")
	}

	jsonbody, err := json.Marshal(claims)
	if err != nil {
		return decodedResult, err
	}

	var obj DecodedStructure
	if err := json.Unmarshal(jsonbody, &obj); err != nil {
		return decodedResult, err
	}

	return obj, nil
}

func CheckToken() func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		// get token (Bearer tokentokentoken)
		tokenString := c.Get("topindotoken")

		// validate token
		_, err := ValidateToken(tokenString)
		if err != nil {
			fmt.Println(err)
			response := model.Response{
				Code:   401,
				Status: "Unauthorized",
				Error: map[string]interface{}{
					"general": "UNAUTHORIZED",
				},
			}
			return c.Status(http.StatusUnauthorized).JSON(response)
		}

		// extract data from token
		decodedRes, err := DecodeToken(tokenString)
		if err != nil {
			fmt.Println(err)
			response := model.Response{
				Code:   401,
				Status: "Unauthorized",
				Error: map[string]interface{}{
					"general": "UNAUTHORIZED",
				},
			}
			return c.Status(http.StatusUnauthorized).JSON(response)
		}

		// set to global var
		c.Locals("currentAgentID", decodedRes.ResellerId)
		return c.Next()
	}
}
